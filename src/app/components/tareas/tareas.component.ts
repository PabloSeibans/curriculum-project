import { Component, OnInit, Input } from '@angular/core';
import { Tarea } from 'src/app/models/task';
import { TareaService } from 'src/app/services/tarea.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {
  @Input()
  task: Tarea;

  constructor(public tareaServicio: TareaService) { }

  ngOnInit(): void {

  }

  deleteTask(task: Tarea) {
    if(confirm('Esta Seguro de Eliminar esta cita?')) {
      this.tareaServicio.deleteTask(task);
    }
  }
}


